package pkg

type ErrSink interface {
	AlertNonFatalError(error)
}
