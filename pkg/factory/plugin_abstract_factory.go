package factory

import (
	"context"
	"errors"

	"edgedata/pkg"
	modechanged "edgedata/pkg/plugins/modeChanged"
	transportinput "edgedata/pkg/plugins/transportInput"
	transportoutput "edgedata/pkg/plugins/transportOutput"
)

type PluginAbstractFactory struct {
	ErrSink pkg.ErrSink
	Logger  pkg.Logger
	UsrCfg  pkg.UserConfig

	MsgPropagator pkg.MsgPropagator
	MsgRetriever  pkg.MsgRetreiver

	HiveMQClient transportinput.HiveMQClient

	TxStore pkg.TxStore

	KafkaClient transportoutput.KafkaClient
	GrpcClient  transportoutput.GRPCClient
}

func (f *PluginAbstractFactory) CreatePlugin(ctx context.Context, name string) (pkg.Plugin, error) {
	var p pkg.Plugin

	switch name {
	case modechanged.Name:
		p = f.NewModeChangedPlugin()
	case transportinput.Name:
		p = f.NewTransportInputPlugin()
	case transportoutput.Name:
		p = f.NewTransportOutputPlugin()
	default:
		return nil, errors.New("plugin name not recognized")
	}

	return p, nil
}

func (f *PluginAbstractFactory) NewModeChangedPlugin() *pkg.AbstractIntermediaryPlugin {
	name := modechanged.Name
	ap := pkg.NewAbstractPlugin(f.Logger, f.ErrSink, name, f.UsrCfg.Enabled(name))
	mcp := modechanged.NewModeChangedPlugin(f.ErrSink, f.TxStore)
	aip := pkg.AbstractIntermediaryPlugin{
		AbstractPlugin: ap,
		MsgPropagator:  f.MsgPropagator,
		MsgRetreiver:   f.MsgRetriever,
		MsgHandler:     mcp,
	}

	return &aip
}

func (f *PluginAbstractFactory) NewTransportInputPlugin() *transportinput.TransportInputPlugin {
	name := transportoutput.Name
	ap := pkg.NewAbstractPlugin(f.Logger, f.ErrSink, name, f.UsrCfg.Enabled(name))
	tip := transportinput.NewTransportInputPlugin(&transportinput.TransportInputPluginParams{
		AbstractPlugin: ap,
		MsgPropagator:  f.MsgPropagator,
		HiveMQClient:   f.HiveMQClient,
		Domains:        []string{"get", "from", "usrCfg"},
	})

	return tip
}

func (f *PluginAbstractFactory) NewTransportOutputPlugin() *transportoutput.TransportOutputPlugin {
	name := transportoutput.Name
	ap := pkg.NewAbstractPlugin(f.Logger, f.ErrSink, name, f.UsrCfg.Enabled(name))
	top := transportoutput.TransportOutputPlugin{
		AbstractPlugin: ap,
		MsgRetreiver:   f.MsgRetriever,
		KafkaClient:    f.KafkaClient,
		GRPCClient:     f.GrpcClient,
	}

	return &top
}
