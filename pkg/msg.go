package pkg

type Message struct {
	jsonData []byte
}

func (m *Message) JSON() []byte {
	return m.jsonData
}
