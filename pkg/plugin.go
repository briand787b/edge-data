package pkg

import (
	"context"
	"sync"
)

//////////////////////////////////////////////////////////////

type Plugin interface {
	IsEnabled() bool // why would a plugin be instantiated if it isn't enabled???
	GetName() string
	Run(ctx context.Context) error
}

//////////////////////////////////////////////////////////////

type AbstractPlugin struct {
	ErrSink ErrSink
	Logger  Logger

	enabled bool
	name    string
}

func NewAbstractPlugin(lgr Logger, es ErrSink, name string, enabled bool) *AbstractPlugin {
	return &AbstractPlugin{
		ErrSink: es,
		Logger:  lgr,
		enabled: enabled,
		name:    name,
	}
}

func (p *AbstractPlugin) GetName() string {
	return p.name
}

func (p *AbstractPlugin) IsEnabled() bool {
	return p.enabled
}

func (p *AbstractPlugin) Info(i ...interface{}) {
	p.Logger.Info("%s: %s", p.name, i)
}

////////////////////////////////////////////////////////////////

type MsgPropagator interface {
	PropagateMsg(ctx context.Context, payload interface{}) error
}

type MsgRetreiver interface {
	RetrieveMsg(ctx context.Context) (*Message, error)
}

////////////////////////////////////////////////////////////////

type MsgHandler interface {
	HandleMsg(ctx context.Context, msg *Message, output chan<- interface{})
}

type AbstractIntermediaryPlugin struct {
	*AbstractPlugin
	MsgPropagator
	MsgRetreiver
	MsgHandler
}

func (p *AbstractIntermediaryPlugin) Run(ctx context.Context) error {
	var wg sync.WaitGroup
	payloadCollector := make(chan interface{})

	go func() {
	OUTERLOOP:
		for {
			select {
			case <-ctx.Done():
				break OUTERLOOP
			default:
				msg, err := p.MsgRetreiver.RetrieveMsg(ctx)
				if err != nil {
					p.ErrSink.AlertNonFatalError(err)
					continue
				}

				wg.Add(1)
				go func() {
					defer wg.Done()
					p.MsgHandler.HandleMsg(ctx, msg, payloadCollector)
				}()
			}
		}

		wg.Done()
	}()

	go func() {
		wg.Wait()
		close(payloadCollector)
	}()

	for payload := range payloadCollector {
		if err := p.MsgPropagator.PropagateMsg(ctx, payload); err != nil {
			p.ErrSink.AlertNonFatalError(err)
		}
	}

	return nil
}
