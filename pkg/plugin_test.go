package pkg_test

import "edgedata/pkg"

// AbstractPlugin should be a `Logger`
var _ pkg.Logger = &pkg.AbstractPlugin{}

// AbstractIntermediaryPlugger should be a `Plugin`
var _ pkg.Plugin = &pkg.AbstractIntermediaryPlugin{}
