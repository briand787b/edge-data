package modechanged

import (
	"context"
	"edgedata/pkg"
)

const Name = "ModeProcessor"

type ModeChangedPlugin struct {
	TransactionStore pkg.TxStore
	ErrSink          pkg.ErrSink
}

func NewModeChangedPlugin(es pkg.ErrSink, txs pkg.TxStore) *ModeChangedPlugin {
	return &ModeChangedPlugin{
		TransactionStore: txs,
		ErrSink:          es,
	}
}

func (p *ModeChangedPlugin) HandleMsg(ctx context.Context, msg *pkg.Message, output chan<- interface{}) {
	// get ID from msg
	var id int

	tx, err := p.TransactionStore.GetTxByID(ctx, id)
	if err != nil {
		p.ErrSink.AlertNonFatalError(err)
		return
	}

	output <- tx
}
