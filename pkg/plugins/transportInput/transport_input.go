package transportinput

import (
	"context"
	"sync"

	"edgedata/pkg"
)

const Name = "TransportOutput"

type TransportInputPlugin struct {
	*pkg.AbstractPlugin
	pkg.MsgPropagator
	HiveMQClient HiveMQClient
	Domains      []string
	Ctx          context.Context
	Mx           *sync.RWMutex
}

type TransportInputPluginParams struct {
	AbstractPlugin *pkg.AbstractPlugin
	MsgPropagator  pkg.MsgPropagator
	HiveMQClient   HiveMQClient
	Domains        []string
}

func NewTransportInputPlugin(params *TransportInputPluginParams) *TransportInputPlugin {
	return &TransportInputPlugin{
		AbstractPlugin: params.AbstractPlugin,
		MsgPropagator:  params.MsgPropagator,
		HiveMQClient:   params.HiveMQClient,
		Domains:        params.Domains,
		Mx:             &sync.RWMutex{},
	}
}

func (p *TransportInputPlugin) Run(ctx context.Context) error {
	p.Mx.Lock()
	p.Ctx = ctx
	p.Mx.Unlock()

	for _, dom := range p.Domains {
		if err := p.HiveMQClient.Subscribe(dom, p.HandleMsg); err != nil {
			return err
		}
	}

	<-ctx.Done()
	return nil
}

func (p *TransportInputPlugin) HandleMsg(topic string, payload []byte) {
	p.Mx.RLock()
	ctx := p.Ctx
	p.Mx.RUnlock()

	// do some transformation on payload

	if err := p.MsgPropagator.PropagateMsg(ctx, payload); err != nil {
		p.ErrSink.AlertNonFatalError(err)
	}
}

//////////////////////////////////////////////////////////////////////////

type HiveMQMsgHandler func(topic string, payload []byte)

type HiveMQClient interface {
	Subscribe(topic string, hndlr HiveMQMsgHandler) error
}
