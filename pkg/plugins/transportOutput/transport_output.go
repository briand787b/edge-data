package transportoutput

import (
	"context"
	"edgedata/pkg"
)

const Name = "TransportInput"

type KafkaClient interface {
	Send(payload []byte)
}

type GRPCClient interface {
	Send(payload []byte)
}

type TransportOutputPlugin struct {
	*pkg.AbstractPlugin
	pkg.MsgRetreiver
	KafkaClient KafkaClient
	GRPCClient  GRPCClient
}

// TODO: Parallelize this method
func (p *TransportOutputPlugin) Run(ctx context.Context) error {
	for {
		select {
		case <-ctx.Done():
			return nil
		default:
			msg, err := p.MsgRetreiver.RetrieveMsg(ctx)
			if err != nil {
				// this plugin is critical, so return error if it can't operate
				return err
			}

			// maybe do transformation here on msg

			p.KafkaClient.Send(msg.JSON())
			p.GRPCClient.Send(msg.JSON())
		}
	}
}
