package pkg

import "context"

// Tx represents a retail transaction
type Tx struct {
	ID int
}

type TxStore interface {
	GetTxByID(ctx context.Context, id int) (*Tx, error)
}
